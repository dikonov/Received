<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ApiLanguageListModel</name>
    <message>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <source>German</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <source>Austrian</source>
        <translation>Autrichien</translation>
    </message>
    <message>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation>Portugais</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation>Espagnol</translation>
    </message>
</context>
<context>
    <name>BrowseByCategoryPageForm.ui</name>
    <message>
        <source>Browse by</source>
        <translation>Explorer par</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation>Top 100</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Recommandé</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
</context>
<context>
    <name>BrowsePageForm.ui</name>
    <message>
        <source>Browse</source>
        <translation>Explorer</translation>
    </message>
</context>
<context>
    <name>DockedAudioPlayerForm.ui</name>
    <message>
        <source>Sleep timer</source>
        <translation>Minuteur</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
</context>
<context>
    <name>FavoritePageForm.ui</name>
    <message>
        <source>http://mystation.com/stream.mp3</source>
        <translation>http://mastation.com/stream.mp3</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>My Station</source>
        <translation>Ma station</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>http://mystation.com/logo.jpg</source>
        <translation>http://mastation.com/logo.jpg</translation>
    </message>
    <message>
        <source>Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <source>Sweden</source>
        <translation>Suède</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <source>Pop</source>
        <translation>Pop</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
</context>
<context>
    <name>FavoritesListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Retirer des favoris</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <source>Edit favorite</source>
        <translation>Éditer le favori</translation>
    </message>
</context>
<context>
    <name>FavoritesPageForm.ui</name>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>From </source>
        <translation></translation>
    </message>
    <message>
        <source>Add Custom</source>
        <translation>Ajouter manuellement une radio</translation>
    </message>
</context>
<context>
    <name>NavigationMenuForm.ui</name>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Show player</source>
        <translation>Afficher le lecteur</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>Explorer</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
</context>
<context>
    <name>PlayerLayoutListModel</name>
    <message>
        <source>Original player</source>
        <translation>Taille d&apos;origine</translation>
    </message>
    <message>
        <source>Small player</source>
        <translation>Taille réduite</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Droping DB</source>
        <translation>Réinitialisation</translation>
    </message>
</context>
<context>
    <name>SettingsPageForm.ui</name>
    <message>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Advanced Options</source>
        <translation>Paramètres avancés</translation>
    </message>
    <message>
        <source>Reset DB</source>
        <translation>Réinitialiser la base de données</translation>
    </message>
    <message>
        <source>API Language:</source>
        <translation>Langue API</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Settings info:&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;À propos des paramètres :&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;All settings are saved when you make a change&lt;/p&gt;</source>
        <translation>&lt;p&gt;Chaque modification est sauvegardée automatiquement.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;Reset DB:&lt;/b&gt; Removes everything in the database and gives you a clean start&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Réinitialisation de la base de données&lt;/b&gt; Nettoie la base de données pour repartir d'un bon pied.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;API Language:&lt;/b&gt; Sets the endpoint to be used for API calls e.g. rad.io for English and radio.de for German&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Langue API&lt;/b&gt; Définit l&apos;extension pour les contacts API, par ex : rad.io pour l&apos;API anglaise, radio.fr pour l&apos;API française...&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Player layout</source>
        <translation>Taille du lecteur</translation>
    </message>
    <message>
        <source>API Options</source>
        <translation>Options de l&apos;API</translation>
    </message>
    <message>
        <source>Basic Options</source>
        <translation>Options de base</translation>
    </message>
</context>
<context>
    <name>StationsListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Retirer des favoris</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <source>Add to favorite</source>
        <translation>Ajouter aux favoris</translation>
    </message>
</context>
<context>
    <name>StationsPageForm.ui</name>
    <message>
        <source>From</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>Search station</source>
        <translation>Rechercher une station</translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation>Top 100</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Recommandé</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Local</translation>
    </message>
</context>
</TS>