import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0
import "./layouts"

CoverBackground {
    CoverDesign {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: coverActionArea.top
        anchors.left: parent.left

        title: AudioPlayer.title
        icon: window.stationData ? window.stationData.stationLogo : ""
    }


    CoverActionList {
        id: coverAction

        CoverAction {
            id: playToggle
            iconSource: AudioPlayer.isPlaying ? "image://theme/icon-cover-pause" : "image://theme/icon-cover-play"
            onTriggered: {
                if(!window.stationData)
                    player.playNext()
                else
                    AudioPlayer.togglePlayback()
            }
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-next-song"
            onTriggered: {
                player.playNext()
            }
        }
    }
}


