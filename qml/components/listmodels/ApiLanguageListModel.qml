import QtQuick 2.0

ListModel {
     id: model
     ListElement { title: qsTr("English"); apiUrl: "http://www.rad.io" }
     ListElement { title: qsTr("German"); apiUrl: "http://www.radio.de" }
     ListElement { title: qsTr("Austrian"); apiUrl: "http://www.radio.at" }
     ListElement { title: qsTr("French"); apiUrl: "http://www.radio.fr" }
     ListElement { title: qsTr("Portuguese"); apiUrl: "http://www.radio.pt" }
     ListElement { title: qsTr("Spanish"); apiUrl: "http://www.radio.es" }
 }
