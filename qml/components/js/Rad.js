.pragma library

function getStationFromRadioJson(jsonData) {
    return {
        radIoId: jsonData.id,
        name: jsonData.name,
        stationLogo: getStationLogoFromRadioJson(jsonData),
        country: jsonData.country,
        genre: jsonData.oneGenre,
        url: getStationStreamFromRadioJson(jsonData)
    };
}

/**
 * Possible data to extract
 *
 * "picture1Name": "t100.png",
 * "picture1TransName": "t100.png",
 * "picture2Name": "",
 * "picture3Name": "",
 * "picture4Name": "t175.png",
 * "picture4TransName": "t175.png",
 * "picture5Name": "",
 * "picture5TransName": "",
 * "picture6Name": "t44.png",
 * "picture6TransName": "t44.png",
 * "pictureBaseURL": "http://static.rad.io/images/broadcasts/e2/a2/10597/",
 */
function getStationLogoFromRadioJson(jsonData) {
    return jsonData.pictureBaseURL+jsonData.picture1Name;
}


/**
 * Get perferred stream from all streams available
 *
 * has no real logic now but should be extended with perferred bitrate and so on
 */
function getStationStreamFromRadioJson(jsonData) {
    var streamURL = jsonData.streamURL;
    for(var i = 0; i < jsonData.streamUrls.length; i++) {
        var streamObj = jsonData.streamUrls[i];
        if(streamObj.streamStatus === "VALID") {
            streamURL = streamObj.streamUrl
        }
        if(streamObj.metaDataUrl) {
            break;
        }
    };

    return streamURL;
}
