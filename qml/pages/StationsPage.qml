import QtQuick 2.6
import "../components/js/Favorites.js" as FavoritesUtils
import "../components/js/Utils.js" as Utils

StationsPageForm {

    // Set properties

    showSearch: listType === Utils.Search


    // Add listeners

    onRemoveFavorite: FavoritesUtils.removeFavorite(station)

    onAddFavorite: radioAPI.addRadIoAsFavorite(id)

    onPlayStation: radioAPI.playStationById(id);

    onSearch: radioAPI.search(text, stationModel);


    // Helper functions

    function getStation(index) {
        return stationModel.get(index);
    }


    // Other

    // Set a custom search action in navigation menu
    navigationMenu.searchAction: function() {
        pageStack.replaceAbove(
                    pageStack.find(function(page) {
                        return page.firstPage === true}),
                    Qt.resolvedUrl("StationsPage.qml"),
                    {listType: Utils.Search});
    }


    // Init

    Component.onCompleted: {
        if(listType === Utils.Top100) {
            radioAPI.getTop100(stationModel);
        }
        else if(listType === Utils.Recommended) {
            radioAPI.getRecomended(stationModel);
        }
        else if(listType === Utils.Local) {
            radioAPI.getLocal(stationModel)
        }
        else if(listType !== Utils.Search) {
            radioAPI.getStationByCategory(category.toLocaleLowerCase(), value, stationModel);
        }
    }
}
