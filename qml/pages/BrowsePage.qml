import QtQuick 2.6
import "../components/js/Utils.js" as Utils

BrowsePageForm {

    browseListView.onCurrentIndexChanged: {
        var item = browseModel.get(browseListView.currentIndex)

        switch(item.action) {
        case "top100":
            pageStack.push(Qt.resolvedUrl("StationsPage.qml"), {listType: Utils.Top100});
            break;
        case "recommended":
            pageStack.push(Qt.resolvedUrl("StationsPage.qml"), {listType: Utils.Recommended});
            break;
        case "localStations":
            pageStack.push(Qt.resolvedUrl("StationsPage.qml"), {listType: Utils.Local});
            break;
        default:
            pageStack.push(Qt.resolvedUrl("BrowseByCategoryPage.qml"), {category: item.originalTitle, headerTitle: item.title});
            break;
        }
    }

    Component.onCompleted: {
        browseModel.append({ title: qsTr("Local"), action: "localStations"})
        browseModel.append({ title: qsTr("Top 100"), action: "top100"})
        browseModel.append({ title: qsTr("Recommended"), action: "recommended"})
        browseModel.append({ title: qsTr("Genre"), action: "browse", originalTitle: "genre"})
        browseModel.append({ title: qsTr("Topic"), action: "browse", originalTitle: "topic"})
        browseModel.append({ title: qsTr("Country"), action: "browse", originalTitle: "country"})
        browseModel.append({ title: qsTr("City"), action: "browse", originalTitle: "city"})
        browseModel.append({ title: qsTr("Language"), action: "browse", originalTitle: "language"})
    }
}
