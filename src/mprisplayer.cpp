#include "mprisplayer.h"
#include <QDebug>
#include <QDBusConnection>
#include <QDBusMessage>


MprisPlayer::MprisPlayer(AudioPlayer *parent) : QDBusAbstractAdaptor(parent)
{
    m_player = parent;
    connect(m_player, &QMediaPlayer::stateChanged, this, &MprisPlayer::playbackStatusChanged);
    connect(m_player, static_cast<void(QMediaPlayer::*)()>(&QMediaPlayer::metaDataChanged),
                this, &MprisPlayer::metaDataChanged);
}

void MprisPlayer::Play()
{
    m_player->startPlayback();
}

void MprisPlayer::PlayPause()
{
    m_player->togglePlayback();
}

void MprisPlayer::Pause()
{
    m_player->pauseOrStopPlayback();
}

void MprisPlayer::Stop()
{
    m_player->stopPlayback();
}

QString MprisPlayer::PlaybackStatus()
{
    switch(m_player->state()) {
    case QMediaPlayer::PlayingState:
        return "Playing";
    case QMediaPlayer::PausedState:
        return "Paused";
    default:
        return "Stopped";
    }
}

qint64 MprisPlayer::Position()
{
    return m_player->position();
}

double MprisPlayer::MinimumRate()
{
    return 1.0;
}

double MprisPlayer::MaximumRate()
{
    return 1.0;
}

double MprisPlayer::Rate()
{
    return m_player->playbackRate();
}

double MprisPlayer::Volume()
{
    return m_player->volume();
}

QString MprisPlayer::LoopStatus()
{
    return "None";
}

QVariantMap MprisPlayer::Metadata()
{
    QVariantMap metadata;
    metadata.insert("mpris:trackid", QString("/org/mpris/MediaPlayer2/Track/%1").arg(1));
    metadata.insert("xesam:title", m_player->title());
    metadata.insert("xesam:genre", m_player->genre());
    // TODO add more metadata
    return metadata;
}

void MprisPlayer::playbackStatusChanged()
{
    QVariantMap changedProps;
    changedProps.insert("PlaybackStatus", PlaybackStatus());
    changedProps.insert("CanPlay", CanPlay());
    notifyPropertyChanged(changedProps);
}

void MprisPlayer::metaDataChanged()
{
    QVariantMap changedProps;
    changedProps.insert("Metadata", Metadata());
    notifyPropertyChanged(changedProps);
}

void MprisPlayer::notifyPropertyChanged(const QVariantMap &changedProps)
{
    QDBusMessage signal = QDBusMessage::createSignal(
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged");
    signal << "org.mpris.MediaPlayer2.Player";
    signal << changedProps;
    signal << QStringList();
    QDBusConnection::sessionBus().send(signal);
}

// Not implemeted function
void MprisPlayer::Next()
{
    qDebug() << "MprisBase: Next not supported";
}

void MprisPlayer::Previous()
{
    qDebug() << "MprisBase: Previous not supported";
}

void MprisPlayer::setRate(const double rate)
{
    qDebug() << "MprisBase: setRate not supported: " << rate;
}

void MprisPlayer::setVolume(const double volume)
{
    qDebug() << "MprisBase: setVolume not supported: " << volume;
}

void MprisPlayer::setLoopStatus(QString loop)
{
    qDebug() << "MprisBase: setLoopStatus not supported: " << loop;
}

void MprisPlayer::setShuffle(const bool suffle)
{
    qDebug() << "MprisBase: setShuffle not supported: " << suffle;
}

void MprisPlayer::OpenUri(QString uri)
{
    qDebug() << "MprisBase: OpenUri not supported: " << uri;
}

void MprisPlayer::Seek(qint64 offset)
{
    qDebug() << "MprisBase: Seek not supported: " << offset;
}

void MprisPlayer::SetPosition(QString trackId, qint64 position)
{
    qDebug() << "MprisBase: SetPosition not supported: " << trackId << " - " << position;
}
